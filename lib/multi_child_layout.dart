import 'package:flutter/material.dart';
import 'dart:math';

class CustomMultiChildWheel extends StatefulWidget {
  CustomMultiChildWheel({Key key}) : super(key: key);

  _CustomMultiChildWheelState createState() => _CustomMultiChildWheelState();
}

class _CustomMultiChildWheelState extends State<CustomMultiChildWheel> {
  List<Widget> children = [];
  double _rotateBy = 0;
  @override
  void initState() {
    super.initState();
    // _formChildren();
  }

  _formChildren() {
    for (int i = 0; i < 10; i++) {
      children.add(LayoutId(
        id: 'Child$i',
        child: Transform.rotate(
          angle: -_rotateBy,
          child: CircleAvatar(
            radius: 30,
            backgroundColor: Colors.red,
            child: InkWell(onTap: () => print('$i pressed'), child: Text('A')),
          ),
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    _formChildren();
    return Scaffold(
        body: GestureDetector(
            onPanUpdate: (panUpdateDetails) {
              final direction = panUpdateDetails.delta.direction;
              final distance = panUpdateDetails.delta.distance / 100;
              final difference = distance * direction;
              setState(() {
                _rotateBy = _rotateBy - difference;
              });
            },
            child: Transform.rotate(
                angle: _rotateBy,
                origin: Offset(MediaQuery.of(context).size.width / 2, 0),
                child: CustomMultiChildLayout(
                  delegate: _CircularLayoutDelegate(itemCount: 10, radius: 200),
                  children: children,
                ))));
  }
}

class _CircularLayoutDelegate extends MultiChildLayoutDelegate {
  static const String actionButton = 'Child';
  Offset center;
  final int itemCount;
  final double radius;

  _CircularLayoutDelegate({
    @required this.itemCount,
    @required this.radius,
  });

  @override
  void performLayout(Size size) {
    const double _radiansPerDegree = pi / 180;
    final double _startAngle = -180.0 * _radiansPerDegree;
    double _itemSpacing = 360.0 / itemCount;

    center = Offset(size.width / 2, size.height / 2);
    for (int i = 0; i < itemCount; i++) {
      final String actionButtonId = 'Child$i';

      double itemAngle = _startAngle + i * _itemSpacing * _radiansPerDegree;
      final Size buttonSize =
          layoutChild(actionButtonId, BoxConstraints.loose(size));
      final offset = Offset(
            (center.dx - buttonSize.width / 2) + (radius) * cos(itemAngle),
            (center.dy - buttonSize.height / 2) + (radius) * sin(itemAngle),
          ) +
          Offset(size.width / 2, 0);

      positionChild(
        actionButtonId,
        offset,
      );
    }
  }

  @override
  bool shouldRelayout(_CircularLayoutDelegate oldDelegate) => true;
}
