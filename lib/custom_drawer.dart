import 'package:flutter/material.dart';

class CustomDrawer extends StatefulWidget {
  CustomDrawer({Key key}) : super(key: key);

  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  final GlobalKey<ScaffoldState> scaffoldStateKey = GlobalKey();
  final GlobalKey _key = GlobalKey();
  bool drawerOpen = false;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // _key.currentContext.size.width
    return Scaffold(
      key: scaffoldStateKey,
      drawer: Container(
        key: _key,
        color: Colors.amberAccent,
        constraints: BoxConstraints(maxWidth: size.width * 3 / 4),
      ),
      body: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        margin: EdgeInsets.only(left: scaffoldStateKey.currentState.isDrawerOpen ? size.width * 3 / 4 : 0),
        color: Colors.white,

      ),
      drawerScrimColor: Colors.transparent,
    );
  }
}
