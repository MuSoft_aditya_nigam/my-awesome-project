import 'package:flutter/material.dart';
import 'package:randomideas/bike_selection_widget.dart';

class CustomBackground extends StatelessWidget {
  const CustomBackground({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xffE87536),
                Color(0xff9A69A7),
                Color(0xff7063E5)
              ]),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: -50,
              top: -50,
              child: Transform.rotate(
                angle: -0.5,
                child: Opacity(
                  child: Image.asset(
                    'assets/bubble.png',
                    scale: 1.9,
                  ),
                  opacity: 0.5,
                ),
              ),
            ),
            Positioned(
              left: -150,
              bottom: -100,
              child: Transform.rotate(
                angle: 0.5,
                child: Opacity(
                  child: Image.asset(
                    'assets/bubble.png',
                    scale: 1.2,
                  ),
                  opacity: 0.4,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
