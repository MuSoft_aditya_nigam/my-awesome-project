import 'package:flutter/material.dart';
import 'dart:math';

class BikeSelectionWidget extends StatefulWidget {
  ///size is needed to determine layout parameters
  final Size size;
  BikeSelectionWidget({Key key, @required this.size})
      : assert(size != null),
        super(key: key);

  _BikeSelectionWidgetState createState() => _BikeSelectionWidgetState();
}

class _BikeSelectionWidgetState extends State<BikeSelectionWidget> {
  final itemCount = 8;
  List<Offset> positions = [];
  List<bool> selected = [];
  final itemRadius = 30.0;
  final centerItemRadius = 80.0;
  int positionSelected;
  Size size;
  @override
  void initState() {
    super.initState();
    size = widget.size;
    _doMath();
  }

  _doMath() {
    final diameter = size.width - 100;
    final sweepAngle = 2 * pi / itemCount;
    for (int i = 0; i < itemCount; i++) {
      final x = (1 + cos(i * sweepAngle)) * diameter / 2;
      final y = (1 + sin(i * sweepAngle)) * diameter / 2;
      final point =
          Offset(x, y) - Offset(size.width / 2, diameter / 2) + Offset(50, 0);
      positions.add(point);
      selected.add(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: BackgroundPainter(),
      child: Stack(
        overflow: Overflow.clip,
        children: <Widget>[
          Positioned(
            left: size.width / 2 - centerItemRadius,
            top: size.height / 2 - centerItemRadius,
            child: Container(
              height: 2 * centerItemRadius,
              width: 2 * centerItemRadius,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.grey[200],
                boxShadow: [
                  BoxShadow(
                      blurRadius: 5, color: Colors.black12, spreadRadius: 3)
                ],
              ),
              child: Center(
                  child: Text(
                'Select Brand',
                style: TextStyle(color: Colors.grey),
              )),
            ),
          ),
          ...positions.map((offset) {
            final currentIndex = positions.indexOf(offset);
            return AnimatedPositioned(
                left: (positionSelected != null) && selected[currentIndex]
                    ? size.width / 2 - centerItemRadius
                    : offset.dx + size.width / 2 - itemRadius,
                top: (positionSelected != null) && selected[currentIndex]
                    ? size.height / 2 - centerItemRadius
                    : offset.dy + size.height / 2 - itemRadius,
                child: GestureDetector(
                  behavior: HitTestBehavior.deferToChild,
                  onTap: () => setState(() {
                    positionSelected = currentIndex;
                    for (int i = 0; i < selected.length; i++) {
                      selected[i] = false;
                    }
                    selected[positionSelected] = true;
                  }),
                  child: AnimatedContainer(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          blurRadius: 5, color: Colors.black26, spreadRadius: 3)
                    ], shape: BoxShape.circle, color: Colors.black),
                    height: (positionSelected != null) && selected[currentIndex]
                        ? 2 * centerItemRadius
                        : 2 * itemRadius,
                    width: (positionSelected != null) && selected[currentIndex]
                        ? 2 * centerItemRadius
                        : 2 * itemRadius,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          (currentIndex).toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        if (currentIndex == 0 && selected[currentIndex])
                          FittedBox(
                            fit: BoxFit.contain,
                            child: Text(
                            'Here goes the TextController',
                            style: TextStyle(color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                          )
                      ],
                    ),
                    duration: Duration(milliseconds: 400),
                  ),
                ),
                duration: Duration(milliseconds: 400));
          }).toList(),
        ],
      ),
    );
  }
}

class BackgroundPainter extends CustomPainter {
  final itemRadius = 30.0;
  BackgroundPainter();
  @override
  void paint(Canvas canvas, Size size) {
    final dashes = 65;

    final double singleAngle = 2 * pi / dashes;
    final double gapSize = 1.0;
    final double gap = pi / 180 * gapSize;

    canvas.translate(size.width / 2, size.height / 2);

    for (int i = 0; i < dashes; i++) {
      final Paint paint = Paint()
        ..color = Colors.white
        ..strokeWidth = 2.0
        ..style = PaintingStyle.stroke;
      canvas.drawArc(
          Rect.fromCircle(center: Offset(0, 0), radius: (size.width - 100) / 2),
          gap + singleAngle * i,
          singleAngle - gap * 2,
          false,
          paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
