import 'package:flutter/material.dart';
import 'package:randomideas/custom_kruz_background.dart';
import 'package:randomideas/bike_selection_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: CustomBackground(),//MyHomePage()//CustomDrawer(),//ItCrowdPage(),//CustomMultiChildWheel(),//CurvedList()//WheelSector()//WheelWidget()// MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  PageController _pageController;
  var currentPosition = 0;
  var animationController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      viewportFraction: 0.2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BikeSelectionWidget(size: MediaQuery.of(context).size,);
    return Scaffold(
        body: PageView.builder(
      itemCount: 10,
      onPageChanged: (newPosition) {
        print('nrewPosition => $newPosition');
        currentPosition = newPosition;
      },
      scrollDirection: Axis.vertical,
      pageSnapping: true,
      itemBuilder: (ctxt, index) {
        return Container(
          height: (index == currentPosition)?150.0:100,
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
          child: Center(child: Text('Index :::: ${index + 1}')),
          decoration: BoxDecoration(
              color: Colors.yellow,
              shape: BoxShape.circle,
              border: Border.all(color: Colors.orange, width: 5.0)),
        );
      },
      controller: _pageController,
    ));
  }

  // double _calculateItemHeight() {
  //   print('currentPage');
  // }
}
