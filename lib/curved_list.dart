import 'package:flutter/material.dart';
import 'package:randomideas/wheelsector.dart';

class CurvedList extends StatefulWidget {
  CurvedList({Key key}) : super(key: key);

  _CurvedListState createState() => _CurvedListState();
}

class _CurvedListState extends State<CurvedList> {
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.red,
        body: WheelSector(
          itemCount: 8,
          givenDiameter: 500,
          canvasSize: MediaQuery.of(context).size,
        ));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
